import sys
import wdr
import wdr.tools
import logging

log = logging.getLogger('script')
sensitiveLog = logging.getLogger('sensitive')

try:
    cellType= getid1('/Cell:/').cellType
    sys.path.append('../manifests')
    sys.path.append('../variables/%s' % sys.argv[0])
    try:
        import topology as tp
        log.info("Topology imported")
    except:
        log.error('failed to import topology')
        sensitiveLog.exception('failed to import topology')
        raise Exception('failed to import topology')
    
    #log.info("Creating Cluster via wdr")
    #wdr.tools.importConfigurationManifest('../manifests/cluster-creation.wdrc', tp.vars['deploy'])
    log.info("Removing Cluster via wdr")
    wdr.tools.importConfigurationManifest('../manifests/cluster-remove.wdrc', tp.vars['deploy'])
    if hasChanges():
       log.info("Changes have been detected, saving them")
       save()
       log.info("Syncronizing the changes")
       sync()
    else:
       log.info("No changes detected")
finally:
   reset()
